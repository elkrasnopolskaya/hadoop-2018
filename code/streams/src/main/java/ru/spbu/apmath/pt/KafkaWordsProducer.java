package ru.spbu.apmath.pt;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class KafkaWordsProducer {
    static class SimpleProducer implements Runnable {
        private final Properties props;
        private final String topic;

        private volatile boolean isStopped;

        public SimpleProducer(final Properties props, final String topic) {
            this.props = props;
            this.topic = topic;
            this.isStopped = false;
        }

        private int randInt(final int b, final int e) {
            return (int) (b + Math.random() * (e - b));
        }

        private void makeWord(final StringBuilder appendTo) {
            for(int i = 1; i <= randInt(1, 5); i++) {
                appendTo.append((char) randInt('a', 'c'));
            }
        }

        private String makeLine() {
            final StringBuilder sb = new StringBuilder();
            final int numWords = randInt(1, 10);
            for(int i = 1; i <= numWords; i++) {
                makeWord(sb);
                if (i < numWords - 1) {
                    sb.append(' ');
                }
            }

            return sb.toString();
        }

        public void run() {
            final KafkaProducer<String, String> producer = new KafkaProducer<String, String>
                    (props, new StringSerializer(), new StringSerializer());

            while(!isStopped) {
                final String text = makeLine();
                final ProducerRecord<String, String> producerRecord =
                        new ProducerRecord<String, String> (topic, "text", text);
                producer.send(producerRecord);

                try {
                    TimeUnit.SECONDS.sleep(randInt(1, 4));
                } catch (InterruptedException e) {
                    // none
                }

            }
            producer.close();
        }
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);

        final Properties props = new KafkaLocalRunner
                .KafkaLocalConfig(Paths.get("/tmp/kafka"))
                .getConsumerProps();
        new Thread(new SimpleProducer(props, "streams-wordcount-output")).start();
    }
}
