package ru.spbu.apmath.pt;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class KafkaWordsConsumer {

    public static class SimpleConsumer implements Runnable {
        private final Properties props;
        private final String topic;

        private volatile boolean isStopped;

        public SimpleConsumer(final Properties props, final String topic) {
            this.props = props;
            this.topic = topic;
            this.isStopped = false;
        }
        @Override
        public void run() {
            final KafkaConsumer<String, String> consumer =
                    new KafkaConsumer<String, String>(props, new StringDeserializer(), new StringDeserializer());

            consumer.subscribe(Collections.singletonList(topic));
            while (!isStopped) {
                final ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));

                for (final ConsumerRecord<String, String> record : records) {
                    System.out.println("Message: (" + record.key() + ", " + record.value() + ") at offset " + record.offset());
                }
            }
        }
    }

    public static void main(String[] args) {
        final Properties props = new KafkaLocalRunner
                .KafkaLocalConfig(Paths.get("/tmp/kafka"))
                .getConsumerProps();
        new Thread(new SimpleConsumer(props, "streams-wordcount-output")).start();
    }
}
