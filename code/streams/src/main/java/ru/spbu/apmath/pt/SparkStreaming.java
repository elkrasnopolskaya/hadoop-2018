package ru.spbu.apmath.pt;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.Seconds;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.SparkConf;
import org.apache.spark.TaskContext;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.kafka010.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import scala.Tuple2;


import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SparkStreaming {
    public static void main(String[] args) throws InterruptedException {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.FATAL);

        final SparkConf conf = new SparkConf().setAppName("Spark streaming example").setMaster("local[*]");
        final JavaStreamingContext ssc = new JavaStreamingContext(conf, Durations.seconds(1));

        final Properties props = new KafkaLocalRunner
                .KafkaLocalConfig(Paths.get("/tmp/kafka"))
                .getConsumerProps();

        final Map<String, Object> propsMap = props.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getKey().toString(),
                        Map.Entry::getValue));


        Collection<String> topics = Arrays.asList("streams-wordcount-output");

        JavaInputDStream<ConsumerRecord<String, String>> stream =
                KafkaUtils.createDirectStream(
                        ssc,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.<String, String>Subscribe(topics, propsMap)
                );


        stream.map(t -> t.value())
                .window(Durations.seconds(3), Durations.seconds(2))
                .flatMap(x -> Arrays.asList(x.split("\\s+")).iterator())
                .countByValue()
                .foreachRDD(rdd -> {
                    System.out.println(
                            rdd.map(t -> t.swap())
                               .sortBy(t -> -t._1, false, 1)
                               .take(5)
                               .toString()
                    );
                });

        ssc.start();
        ssc.awaitTermination();
    }
}
