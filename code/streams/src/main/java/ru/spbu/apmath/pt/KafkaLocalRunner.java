package ru.spbu.apmath.pt;

import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.server.KafkaServerStartable;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.apache.commons.io.FileUtils;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

public class KafkaLocalRunner {
    public static final int BROKER_PORT = 5000;
    public static final int ZOOKEEPER_PORT = 2000;

    private static KafkaLocal runKafka(final Path logDir) throws Exception {
        KafkaLocalConfig cfg = new KafkaLocalConfig(BROKER_PORT, ZOOKEEPER_PORT, logDir);
        KafkaLocal kafka = new KafkaLocal(cfg);
        kafka.start();

        return kafka;
    }

    private static void deleteRecursively(final File file) {
        if (file.isDirectory())
            Arrays.stream(file.listFiles()).forEach(KafkaLocalRunner::deleteRecursively);
    }

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);

        Path tmpFolder = Files.createTempDirectory("kafka");
        FileUtils.forceDeleteOnExit(tmpFolder.toFile());
        runKafka(tmpFolder);
    }


    static class ZooKeeperLocal {
        private final Properties zkProperties;

        ZooKeeperLocal(final Properties zkProperties) {
            this.zkProperties = zkProperties;
        }

        void start() throws Exception {
            final QuorumPeerConfig quorumConfiguration = new QuorumPeerConfig();
            quorumConfiguration.parseProperties(zkProperties);

            final ZooKeeperServerMain zooKeeperServer = new ZooKeeperServerMain();
            final ServerConfig configuration = new ServerConfig();
            configuration.readFrom(quorumConfiguration);

            new Thread(() -> {
                try {
                    zooKeeperServer.runFromConfig(configuration);
                } catch (IOException ex) {
                    ex.printStackTrace(System.err);
                }
            }).start();

        }
    }


    static class KafkaLocal {
        private final KafkaLocalConfig cfg;

        KafkaLocal(final KafkaLocalConfig cfg) {
            this.cfg = cfg;
        }

        void start() throws Exception {
            final ZooKeeperLocal zoo = new ZooKeeperLocal(cfg.getZookeeperProperties());
            zoo.start();

            final KafkaServerStartable kafka = KafkaServerStartable.fromProps(cfg.getKafkaProps());
            kafka.startup();
        }
    }

    public static class KafkaLocalConfig {
        private final int port;
        private final int zkPort;

        private final Path logDir;

        private final Path kLogDir;
        private final Path zkLogDir;

        public KafkaLocalConfig(final Path logDir) {
            this(BROKER_PORT, ZOOKEEPER_PORT, logDir);
        }

        public KafkaLocalConfig(final int port, final int zkPort, final Path logDir) {
            this.logDir = logDir;
            this.port = port;
            this.zkPort = zkPort;
            this.kLogDir = logDir.resolve("kafka");
            this.zkLogDir = logDir.resolve("zookeeper");
        }

        public Properties getConsumerProps() {
            final Properties props = new Properties();

            props.put(ENABLE_AUTO_COMMIT_CONFIG, "true");
            props.put(AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
            props.put(SESSION_TIMEOUT_MS_CONFIG, "30000");
            props.put(GROUP_ID_CONFIG, "1");
            props.put(KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
            props.put(VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
            props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:" + port);

            props.put(KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
            props.put(VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

            return props;
        }

        Properties getKafkaProps() {
            final Properties props = new Properties();

            props.put("port", port + "");
            props.put("broker.id", "0");
            props.put("log.dir", kLogDir.toString());
            props.put("zookeeper.connect", "localhost:" + zkPort);
            props.put("default.replication.factor", "1");
            props.put("offsets.topic.replication.factor", "1");
            props.put("delete.topic.enable", "true");
            return props;
        }

        public void createTopic(final String topicName) {
            AdminClient zkUtils = AdminClient.create(getConsumerProps());
            zkUtils.createTopics(Collections.singleton(new NewTopic(topicName, 1, (short)1)));
        }

        Properties getZookeeperProperties() {
            final Properties props = new Properties();

            props.put("clientPort", zkPort + "");
            props.put("dataDir", zkLogDir);
            return props;
        }
    }
}




