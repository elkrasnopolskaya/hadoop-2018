package ru.spbu.apmath.pt;


import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Pattern;

public class KafkaStreamsWordCount {

    private static final String INPUT_TOPIC_NAME = "text-topic";
    private static final String OUTPUT_TOPIC_NAME = "words-topic";

    private static void runProducer() {
        final KafkaLocalRunner.KafkaLocalConfig cfg = new KafkaLocalRunner
                .KafkaLocalConfig(Paths.get("/tmp/kafka"));

        cfg.createTopic(OUTPUT_TOPIC_NAME);
        new Thread(new KafkaWordsProducer.SimpleProducer(cfg.getConsumerProps(), INPUT_TOPIC_NAME)).start();
        new Thread(new KafkaWordsConsumer.SimpleConsumer(cfg.getConsumerProps(), OUTPUT_TOPIC_NAME)).start();
    }


    public static void main(final String[] args) throws Exception {
        runProducer();

        final String bootstrapServers = args.length > 0 ? args[0] : "localhost:5000";
        final Properties streamsConfiguration = new Properties();

        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "wordcount-lambda-example");
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG, "wordcount-lambda-example-client");

        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);


        final Serde<String> stringSerde = Serdes.String();

        final StreamsBuilder builder = new StreamsBuilder();
        final KStream<String, String> textLines = builder.stream(INPUT_TOPIC_NAME);

        final Pattern pattern = Pattern.compile("\\W+", Pattern.UNICODE_CHARACTER_CLASS);

        final KTable<String, Long> wordCounts = textLines
                .flatMapValues(value -> Arrays.asList(pattern.split(value.toLowerCase())))
                .groupBy((key, word) -> word)
                .count();

        wordCounts
                .toStream()
                .mapValues(value -> "" + value)
                .to(OUTPUT_TOPIC_NAME, Produced.with(stringSerde, stringSerde));

        final KafkaStreams streams = new KafkaStreams(builder.build(), streamsConfiguration);

        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }
}
