from mrjob.job import MRJob
from mrjob.protocol import TextProtocol
import re

WORD_RE = re.compile(r"[a-zA-Zа-яА-Я]+\.[a-zA-Zа-яА-Я]\.")

threshold_count = 15

class MRWordFreqCount(MRJob):
    OUTPUT_PROTOCOL = TextProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            if len(word) == 4:
                yield word.lower(), 1

    def combiner(self, word, counts):
        yield word, sum(counts)

    def reducer(self, word, counts):
        sum_count = sum(counts)
        if sum_count > threshold_count:
            yield word, str(sum_count)


if __name__ == '__main__':
    MRWordFreqCount.run()