from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[a-zA-Zа-яА-Я]+[.]*")

threshold_per = 0.8
threshold_count = 50


class MRWordFreqCount(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            word_low = word.lower()
            if word[-1] == '.':
                yield word_low[:-1], True
            else:
                yield word_low, False

    def reducer(self, word, counts):
        count_with_dot = 0
        count_all = 0
        for count in counts:
            count_all += 1
            count_with_dot += 1 if count else 0
        if count_all > threshold_count and count_with_dot > count_all*threshold_per:
            yield word + '.', (str(count_with_dot)+"/"+str(count_all))


if __name__ == '__main__':
    MRWordFreqCount.run()