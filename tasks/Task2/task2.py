from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[a-zA-Zа-яА-Я]+")


class MRWordFreqCount(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            yield None, len(word)

    def reducer(self, _, lengths):
        count_words = 0
        count_letters = 0
        for length in lengths:
            count_words += 1
            count_letters += length
        yield "Average length", count_letters/count_words

if __name__ == '__main__':
    MRWordFreqCount.run()