from mrjob.job import MRJob
from mrjob.protocol import ReprProtocol
import re

WORD_RE = re.compile(r"[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я]+")


class MRWordFreqCount(MRJob):
    OUTPUT_PROTOCOL = ReprProtocol

    def mapper(self, _, line):
        for word in WORD_RE.findall(line):
            word_low = word.lower()
            if word[0] == word_low[0]:
                yield word_low, False
            else:
                yield word_low, True

    def reducer(self, word, counts):
        count_capital = 0
        count_all = 0
        for count in counts:
            count_all += 1
            count_capital += 1 if count else 0
        if count_all > 10 and count_capital*2 > count_all:
            yield word, (str(count_capital)+"/"+str(count_all))


if __name__ == '__main__':
    MRWordFreqCount.run()